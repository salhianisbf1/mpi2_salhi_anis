# Use the official Nginx base image
FROM nginx:alpine

# Set the working directory in the container
WORKDIR /usr/share/nginx/html

# Copy all files from the current directory to the working directory
COPY . .

# Expose port 80
EXPOSE 80

# The CMD does not need to be specified here, as the default CMD of the Nginx image is to start Nginx

