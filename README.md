# MPI2_Salhi_Anis

## Deploy to Kubernetes

Création de pods, déploiements et services Kubernetes
<img src="https://gitlab.com/salhianisbf1/mpi2_salhi_anis/-/blob/main/imgs/k8s_1.png">

<img src="https://gitlab.com/salhianisbf1/mpi2_salhi_anis/-/blob/main/imgs/k8s_2.png" />

Automatiser le déploiement et la gestion des applications dans les clusters Kubernetes avec ArgoCD

<img src="https://gitlab.com/salhianisbf1/mpi2_salhi_anis/-/blob/main/imgs/ArgoCD.png" />
